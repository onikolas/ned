package main

import "bitbucket.org/tentontrain/math"

type ActionResizeWindow struct {
	window   string
	parent   math.Box
	fontSize math.Point
}

func NewActionResizeWindow(parent math.Box, fontSize math.Point) EditorAction {
	return &ActionResizeWindow{parent: parent, fontSize: fontSize}
}

func (a *ActionResizeWindow) Do(e *Editor, in []interface{}) []interface{} {
	e.Windows[a.window].UpdateGeometry(a.parent, a.fontSize)
	return nil
}

func (a *ActionResizeWindow) SetWindow(window string) {
	a.window = window
}

func (a *ActionResizeWindow) String() string {
	return "ActionResizeWindow"
}
