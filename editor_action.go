// Editor actions are data types that can alter the state of the editor. By making these into
// data (instead of just functions) we can store (buffer) them and undo them.
// If an action is undoable it must add itself to the undo buffer when Do() is called.
package main

type EditorAction interface {
	Do(e *Editor, input []interface{}) []interface{} // main execution
	SetWindow(window string)                         // register originating window
	String() string                                  // convinience
}

// An editor action that can be undone
type Undoer interface {
	Undo(e *Editor)
}
