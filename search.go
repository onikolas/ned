// Search tools for gap
package main

// Search for rune starting at pos and going forward. Returns the first match position  or -1 if not found
func (gb *GapBuffer) SearchRune(r rune, pos int) int {
	if pos < 0 || pos >= gb.Length() {
		return -1
	}

	for i := pos; i < gb.Length(); i++ {
		if gb.At(i) == r {
			return i
		}
	}

	return -1
}

// Search for rune starting at pos and going backwards. Returns the first match position or -1 if not found
func (gb *GapBuffer) SearchRuneReverse(r rune, pos int) int {
	if pos < 0 || pos >= gb.Length() {
		return -1
	}

	for i := pos; i >= 0; i-- {
		if gb.At(i) == r {
			return i
		}
	}

	return -1
}

// Returns the position of the start of the current line
func (gb *GapBuffer) FindLineStart(pos int) int {
	if pos <= 0 {
		return 0
	}
	prev := gb.ConstrainIndex(pos - 1) //avoid self hits if pos is already a newline
	if i := gb.SearchRuneReverse('\n', prev); i >= 0 {
		return i + 1
	}
	return 0
}

// Returns the position of the start of the next line.
func (gb *GapBuffer) FindNextLineStart(pos int) int {
	if i := gb.SearchRune('\n', pos); i >= 0 {
		return gb.ConstrainIndex(i + 1)
	} else {
		return -1
	}
}
