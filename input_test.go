package main

import "testing"

func TestInputInit(t *testing.T) {
	in := InputBuffer{}

	in.Init(3, WindowEdit)

	if cap(in.buffer) != 3 {
		t.Error(cap(in.buffer))
	}
}

func TestInputBuffer(t *testing.T) {
	in := InputBuffer{}
	in.Init(3, WindowEdit)

	k := KeyPress{Key: KeyControlDown}
	in.Add(k)

	if cap(in.buffer) != 3 || len(in.buffer) != 1 {
		t.Error(cap(in.buffer), len(in.buffer))
	}

	for i := 0; i < 5; i++ {
		in.Add(k)
	}

	if cap(in.buffer) != 6 || len(in.buffer) != 6 {
		t.Error(cap(in.buffer), len(in.buffer))
	}

	in.Empty()

	if cap(in.buffer) != 6 || len(in.buffer) != 0 {
		t.Error(cap(in.buffer), len(in.buffer))
	}

}

/*func TestInputAdd(t *testing.T) {
	in := InputBuffer{}
	in.Init(3)

	in.syntaxTree.Add([]KeyPress{{KeyControlDown, ""}, {KeyText, "c"}}, nil)
	in.syntaxTree.Add([]KeyPress{{KeyControlDown, ""}, {KeyText, "v"}}, nil)
	in.syntaxTree.Add([]KeyPress{{KeyControlDown, ""}, {KeyText, "c"}, {KeyText, "a"}}, NewActionInsertTextAtPos("a", 0))
	in.syntaxTree.Add([]KeyPress{{KeyAltDown, ""}}, NewActionInsertTextAtPos("asdasd", 0))

	if action, complete := in.Add(KeyPress{KeyControlDown, ""}, "default"); action != nil || complete {
		t.Error(action, complete)
	}
	if action, complete := in.Add(KeyPress{KeyText, "c"}, "default"); action != nil || complete {
		t.Error(action, complete)
	}
	// last keypress should return an action
	if action, complete := in.Add(KeyPress{KeyText, "a"}, "default"); action == nil || !complete {
		t.Error(action, complete)
	}

	if len(in.buffer) != 0 {
		t.Error("buffer not cleared")
	}

	if action, complete := in.Add(KeyPress{KeyText, "b"}, "default"); action == nil || !complete {
		t.Error(action, complete)
	}
    }*/
