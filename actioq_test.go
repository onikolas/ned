package main

import "testing"

func TestActionQueue(t *testing.T) {
	q := NewActionQueue()
	q.Queue(NewActionInsertDummyText())

	if len(q.actions) != 2 {
		t.Error(q.actions)
	}

	a, err := q.DeQueue()
	_, ok := a.(*ActionInsertTextAtPos)
	if !ok || err != nil {
		t.Error(a, err)
	}

	q.Queue(NewActionInsertTextAtCursor([]rune("arst")))
	q.Queue(NewActionDeleteForward())

	a, err = q.DeQueue()
	_, ok = a.(*ActionInsertTextAtCursor)
	if !ok || err != nil {
		t.Error(a, err)
	}

	a, err = q.DeQueue()
	_, ok = a.(*ActionDeleteForward)
	if !ok || err != nil {
		t.Error(a, err)
	}

	a, err = q.DeQueue()
	if err == nil || a != nil {
		t.Error(a, err)
	}
}
