// Implementation of a gap buffer.
// TODO:
//  - Consider reducing gap size (if bigger than text - min chunk size)
//  - Resizing strategy (double the chunk?)

package main

import (
	"bitbucket.org/tentontrain/math"
)

// Gap buffer
type GapBuffer struct {
	buffer           []rune // storage
	gapStart, gapEnd int    // gap indices
	resizeChunk      int    // how many entries to add when adding more space
}

const DefaultChunkSize = 256

func NewGapBuffer(size int) *GapBuffer {
	gb := &GapBuffer{}
	gb.Init(size)
	return gb
}

// Initialize a Gap to given size
func (gb *GapBuffer) Init(size int) {
	if gb.buffer != nil {
		panic("cannot re-initialize gap buffer")
	}

	gb.buffer = make([]rune, size)
	gb.gapStart = 0
	gb.gapEnd = size
	gb.resizeChunk = DefaultChunkSize
}

// Set how much space to add when buffer becomes full
func (gb *GapBuffer) SetResizeChunk(c int) {
	if c < 1 {
		c = 1
	}
	gb.resizeChunk = c
}

// Text returns slices with the all text
func (gb GapBuffer) Text() ([]rune, []rune) {
	return gb.buffer[0:gb.gapStart], gb.buffer[gb.gapEnd:]
}

// Insert adds text to the buffer at pos (pos is in user coordinates which ignore gap)
func (gb *GapBuffer) Insert(text []rune, pos int) bool {
	if len(text) > gb.GapSize() {
		if gb.resizeChunk < len(text) {
			gb.resizeChunk = len(text)
		}
		gb.Resize(gb.resizeChunk)
	}

	if !gb.ValidPos(pos) {
		// TODO consioder undoing the resize
		return false
	}

	// move the gap if needed
	if pos != gb.gapStart {
		gb.MoveGap(pos)
	}

	for i := range text {
		gb.buffer[pos+i] = text[i]
	}
	gb.gapStart += len(text)

	return true
}

// InsertAtCursor adds text at the current gap position
func (gb *GapBuffer) InsertAtCursor(text []rune) bool {
	return gb.Insert(text, gb.gapStart)
}

// Move the gap to pos. Will cause shifting proportional to the movement: O(pos-gapStart)
func (gb *GapBuffer) MoveGap(pos int) bool {

	if pos < 0 || pos > len(gb.buffer)-gb.GapSize() {
		return false
	}

	move := pos - gb.gapStart
	newS := pos
	newE := pos + gb.GapSize()

	if move < 0 {
		absmove := abs(move)
		for i := 0; i < absmove; i++ {
			gb.buffer[gb.gapEnd-1-i] = gb.buffer[newS+absmove-1-i]
		}
	} else {
		for i := 0; i < move; i++ {
			gb.buffer[gb.gapStart+i] = gb.buffer[gb.gapEnd+i]
		}
	}

	gb.gapStart = newS
	gb.gapEnd = newE

	return true
}

// Resize changes the size of the gap. Positive values increase and negative values decrease.
// Will NOT delete text - only the gap can be reduced.
func (gb *GapBuffer) Resize(size int) {

	// check valid size - cannot deacrease below length of text
	if gs := gb.GapSize(); size < 0 && abs(size) > gs {
		size = -gs
	}

	newBuffer := make([]rune, len(gb.buffer)+size)

	for i := 0; i < gb.gapStart; i++ {
		newBuffer[i] = gb.buffer[i]
	}
	for i := gb.gapEnd; i < len(gb.buffer); i++ {
		newBuffer[i+size] = gb.buffer[i]
	}

	gb.buffer = newBuffer
	gb.gapEnd += size
}

// Delete a num of characters (backspace). Will delete at current gap pos.
// Use MoveGap to delete at specific index.
func (gb *GapBuffer) DeleteBack(num uint) {
	gb.gapStart -= int(num)
	if gb.gapStart < 0 {
		gb.gapStart = 0
	}
}

// Delete a num of characters (del). Will delete at current gap pos.
// Use MoveGap to delete at specific index.
func (gb *GapBuffer) DeleteForward(num uint) {
	gb.gapEnd += int(num)
	if gb.gapEnd >= len(gb.buffer) {
		gb.gapEnd = len(gb.buffer) - 1
	}
}

// Clear all text - will not affect size of allocated buffer (use Resize for that)
func (gb *GapBuffer) DeleteAll() {
	gb.gapStart, gb.gapEnd = 0, len(gb.buffer)
}

// Is the position valid for inserting text
func (gb *GapBuffer) ValidPos(pos int) bool {
	return pos >= 0 && pos < len(gb.buffer)
}

func (gb *GapBuffer) GapSize() int {
	return gb.gapEnd - gb.gapStart
}

// String implementation for Stringer. Does not print contents of gap.
func (gb GapBuffer) String() string {
	return string(gb.buffer[0:gb.gapStart]) + string(gb.buffer[gb.gapEnd:])
}

// Debug buffer print. Prints gap contents as well as the stored string.
func (gb GapBuffer) DebugString() string {
	return string(gb.buffer)
}

// Length of the text
func (gb GapBuffer) Length() int {
	return len(gb.buffer) - gb.GapSize()
}

// Return rune at position i (user-index). WARNING: no bound checks!
func (gb GapBuffer) At(i int) rune {
	if i < gb.gapStart {
		return gb.buffer[i]
	} else {
		return gb.buffer[i+gb.GapSize()]
	}
}

// Get a region of text defined by two indices [a,b).
// Returns two slices to avoid concatenation
func (gb GapBuffer) Regions(a, b int) ([]rune, []rune) {
	a = gb.ConstrainIndex(a)
	if b > gb.Length() {
		b = gb.Length()
	}
	if a < gb.gapStart {
		if b <= gb.gapStart {
			return gb.buffer[a:b], []rune{}
		}
		return gb.buffer[a:gb.gapStart], gb.buffer[gb.gapEnd : gb.gapEnd+b-gb.gapStart]
	}
	return []rune{}, gb.buffer[a+gb.GapSize() : b+gb.GapSize()]
}

// Contrain an index to [0, textLength) so that it always points to a valid text position.
func (gb GapBuffer) ConstrainIndex(index int) int {
	return math.Bound(index, 0, gb.Length()-1)
}
