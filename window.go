package main

import (
	"bitbucket.org/tentontrain/math"
)

type WindowType int

//go:generate stringer -type=WindowType
const (
	WindowEdit     WindowType = iota // displays an editable buffer that can point to a file
	WindowInput                      // returns ist contents via the out chanel
	WindowViewOnly                   // used to show information or open a file in view only mode
	WindowLayout                     // used to arrange other windows - not visible
	NumWindowTypes                   // For itteration
)

// A window is a sub-area in the main window. Windows can overlap. Each window displays a text buffer (a window can switch buffers)
// and is responsible for handling input (different windows can handle input differently)
type Window struct {
	Type     WindowType
	Children []*Window //subwindows

	// Rendering
	WindowGeometry WindowGeometry // controls window size and position
	Background     int            // sprite index of background to use (get it from atlas map)
	Depth          int            // back to front order (higher is in front)

	// Content
	Title     string      // shows loaded filename or description
	Text      string      // name of text buffer
	TextStart int         // index in text buf to start drawing from
	TextEnd   int         // index of the last rune rendered
	Input     InputBuffer // buffers and parses input into editor actions
}

func NewWindow(winType WindowType, name, text string, wg WindowGeometry, background, depth int) *Window {

	w := &Window{
		Type:           winType,
		Children:       []*Window{},
		WindowGeometry: wg,
		Background:     background,
		Depth:          depth,
		Text:           text,
		TextStart:      0,
		TextEnd:        0,
	}
	w.Input.Init(256, winType)
	return w
}

func (w *Window) AddChildWindow(window *Window) {
	w.Children = append(w.Children, window)
}

// Turn a point in the window to screen coordinates
func (w *Window) ToScreen(point math.Point) math.Point {
	return point.Add(w.WindowGeometry.BoundingBox.P1)
}

// UpdateGeometry resizes and repositions a window when parent's geometry changes
func (w *Window) UpdateGeometry(parentBoundingBox math.Box, fontDims math.Point) {
	w.WindowGeometry.Update(parentBoundingBox, fontDims)
	for _, c := range w.Children {
		c.UpdateGeometry(w.WindowGeometry.BoundingBox, fontDims)
	}
}
