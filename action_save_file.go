package main

import (
	"fmt"
	"os"
)

// Load a file from disk
type ActionSaveFile struct {
	window string
}

// Save the file currently open in the window
func NewActionSaveFile() EditorAction {
	return &ActionSaveFile{}
}

func (a *ActionSaveFile) Do(e *Editor, in []interface{}) []interface{} {
	filename := e.Windows[a.window].Text // buffername is filename by convention
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println(err)
	}
	file.WriteString(e.Text[e.Windows[a.window].Text].String()) // TODO
	return nil
}

func (a *ActionSaveFile) SetWindow(w string) {
	a.window = w
}

func (a *ActionSaveFile) String() string {
	return "ActionSaveFile"
}
