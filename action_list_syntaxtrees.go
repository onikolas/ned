// Display syntax trees
package main

import "fmt"

type ActionListSyntaxTrees int

func NewActionListSyntaxTrees() EditorAction {
	return ActionListSyntaxTrees(41)
}

func (a ActionListSyntaxTrees) Do(e *Editor, input []interface{}) []interface{} {
	e.Windows["default"].Text = "default"
	buf := e.Text["default"]
	buf.DeleteAll()
	for name, v := range e.Windows {
		if v.Type == WindowViewOnly {
			continue
		}
		buf.InsertAtCursor([]rune(" * " + name + "\n"))
		buf.InsertAtCursor([]rune(fmt.Sprint(v.Input.syntaxTree)))
	}
	return nil
}

func (a ActionListSyntaxTrees) SetWindow(w string) {}

func (a ActionListSyntaxTrees) String() string {
	return fmt.Sprint("ActionListSyntaxTrees")
}
