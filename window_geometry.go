package main

import "bitbucket.org/tentontrain/math"

type AncorType int

const (
	AncorTypePercent   AncorType = iota // ancors will have values 0-100 which is the percentage of their parent's w/h
	AncorTypePixel                      // absolute values in pixels
	AncorTypeHStretch                   // Specify height and fully fill the width
	AncorTypeTextWidth                  // This ancor moves so that the text in the window fits
)

type WindowGeometry struct {
	BoundingBox                             math.Box   // lower left and upper right points in pixels
	AncorLowerLeft, AncorUpperRight         math.Point // these align the window to the parent window
	AncorTypeLowerLeft, AncorTypeUpperRight AncorType
}

// UpdateGeometry resizes and repositions a window when parent's geometry changes
// Font dimentions are only needed for wondows that change to match the displayed
func (w *WindowGeometry) Update(parentBoundingBox math.Box, fontDims math.Point) {
	ps := parentBoundingBox.Size()
	p := parentBoundingBox.P1

	if w.AncorTypeLowerLeft == AncorTypePercent {
		w.BoundingBox.P1 = p.Add(ps.Mul(w.AncorLowerLeft).Div(math.Point{X: 100, Y: 100}))
	} else {
		w.BoundingBox.P1 = parentBoundingBox.P1.Add(w.AncorLowerLeft)
	}

	switch w.AncorTypeUpperRight {
	case AncorTypePercent:
		w.BoundingBox.P2 = p.Add(ps.Mul(w.AncorUpperRight).Div(math.Point{X: 100, Y: 100}))
	case AncorTypePixel:
		w.BoundingBox.P2 = parentBoundingBox.P2.Add(w.AncorUpperRight)
	case AncorTypeHStretch:
		w.BoundingBox.P2.X = parentBoundingBox.P2.X
		w.BoundingBox.P2.Y = w.BoundingBox.P1.Y + w.AncorUpperRight.Y
	}

	w.BoundingBox.CropToFitIn(parentBoundingBox)
}
