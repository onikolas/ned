// A simple syntax tree
package main

import (
	"fmt"
	"strings"
)

type TreeNode struct {
	Key       KeyPress
	Children  []*TreeNode
	NewAction func() EditorAction // Creates new editor action
}

func NewTreeNode(key KeyPress, action func() EditorAction) *TreeNode {
	tn := TreeNode{}
	tn.Key = key
	tn.NewAction = action
	tn.Children = make([]*TreeNode, 0)
	return &tn
}

type SyntaxTree struct {
	Root *TreeNode
}

type SyntaxMatch int

const (
	MatchNone SyntaxMatch = iota
	MatchPartial
	MatchFull
)

func NewSyntaxTree(wt WindowType) SyntaxTree {
	st := SyntaxTree{}
	none := KeyPress{Key: KeyNone}
	st.Root = NewTreeNode(none, nil)
	st.SetActions(wt)
	return st
}

func NewSyntaxTreeEmpty() SyntaxTree {
	st := SyntaxTree{}
	none := KeyPress{Key: KeyNone}
	st.Root = NewTreeNode(none, nil)
	return st
}

// Search the syntax tree for a (possible partial) match of sequence and return the last matching node.
// Returns the root if no match is found. The index into the sequence is also returned to aid with
// partial matches.
func (st *SyntaxTree) Search(sequence []KeyPress) (*TreeNode, int) {
	node := st.Root
	seqIndex := 0
	for seqIndex < len(sequence) && node.NewAction == nil {
		deadend := true
		for _, c := range node.Children {
			if sequence[seqIndex].Key == c.Key.Key && sequence[seqIndex].Text == c.Key.Text {
				seqIndex++
				node = c
				deadend = false
				break
			}
		}
		if deadend {
			break
		}
	}
	return node, seqIndex
}

// Add a key sequence to the tree. Action is added to the last key.
// If the sequence already exists its action will be updated
func (st *SyntaxTree) Add(sequence []KeyPress, action func() EditorAction) {
	// search for possible match (full or partial)
	node, seqIndex := st.Search(sequence)

	// add the rest of the pattern to the tree
	for i := seqIndex; i < len(sequence); i++ {
		newnode := NewTreeNode(sequence[i], nil)
		node.Children = append(node.Children, newnode)
		node = newnode
	}

	// action is added to the leaf node
	node.NewAction = action
}

// Parse goes through the tree and retrieves the action matching the sequence
func (st *SyntaxTree) Parse(sequence []KeyPress) (EditorAction, SyntaxMatch) {
	actionNode, numMatches := st.Search(sequence)
	if actionNode.NewAction != nil {
		return actionNode.NewAction(), MatchFull
	}
	if numMatches == len(sequence) {
		return nil, MatchPartial
	}
	return nil, MatchNone
}

// SetActions populates the syntax tree. Now hardcoded but should load from a config file eventually
func (st *SyntaxTree) SetActions(wt WindowType) {
	// common actions
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "z"}}, NewActionUndo)
	st.Add([]KeyPress{{Key: KeyLeft, Text: ""}}, NewActionMoveCursorLeft)
	st.Add([]KeyPress{{Key: KeyRight, Text: ""}}, NewActionMoveCursorRight)
	st.Add([]KeyPress{{Key: KeyBackspace, Text: ""}}, NewActionDeleteBackwards)
	st.Add([]KeyPress{{Key: KeyDelete, Text: ""}}, NewActionDeleteForward)
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "d"}}, NewActionDeleteForward)
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""},
		{Key: KeyText, Text: "q"}, {Key: KeyText, Text: "n"}}, NewActionQuit)

	switch wt {
	case WindowInput:
		st.Add([]KeyPress{{Key: KeyNewline, Text: ""}}, NewActionBufferContents)
	case WindowEdit:
		st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""},
			{Key: KeyText, Text: "o"}, {Key: KeyText, Text: "f"}}, NewActionOpenFile)
		st.Add([]KeyPress{{Key: KeyUp, Text: ""}}, NewActionMoveCursorUp)
		st.Add([]KeyPress{{Key: KeyDown, Text: ""}}, NewActionMoveCursorDown)
		st.Add([]KeyPress{{Key: KeyNewline, Text: ""}}, NewActionInsertNewlineAtCursor)
		st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""},
			{Key: KeyText, Text: "l"}, {Key: KeyText, Text: "b"}}, NewActionListBuffers)
		st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""},
			{Key: KeyText, Text: "l"}, {Key: KeyText, Text: "s"}, {Key: KeyText, Text: "t"}}, NewActionListSyntaxTrees)
		st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""},
			{Key: KeyText, Text: "s"}, {Key: KeyText, Text: "b"}}, NewActionSwitchBuffer)
		st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""},
			{Key: KeyText, Text: "s"}, {Key: KeyText, Text: "f"}}, NewActionSaveFile)
		st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""},
			{Key: KeyText, Text: "d"}, {Key: KeyText, Text: "d"}, {Key: KeyText, Text: "d"}}, NewActionDebugDumbDumps)
		st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""},
			{Key: KeyText, Text: "i"}, {Key: KeyText, Text: "w"}}, NewActionInputWindow)
	}
}

func (st SyntaxTree) String() string {
	var sb strings.Builder
	st.Walk(st.Root, 0, &sb)
	return sb.String()
}

func (st SyntaxTree) Walk(node *TreeNode, tabs int, sb *strings.Builder) {
	for i := 0; i < tabs; i++ {
		sb.WriteString(" - ")
	}
	fmt.Fprint(sb, node.Key.Key, node.Key.Text)
	if node.NewAction != nil {
		fmt.Fprint(sb, ": ", node.NewAction())
	}
	sb.WriteString("\n")
	for _, child := range node.Children {
		st.Walk(child, tabs+1, sb)
	}
}
