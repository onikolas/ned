package main

import (
	"bitbucket.org/tentontrain/math"
)

type ActionInputWindow struct {
	window string
}

func NewActionInputWindow() EditorAction {
	return &ActionInputWindow{}
}

func (a *ActionInputWindow) Do(e *Editor, input []interface{}) []interface{} {
	fontDims := math.Point{e.Graphics.TextStore.FontAtlas.HSpace, e.Graphics.TextStore.FontAtlas.VSpace}
	wg := WindowGeometry{
		AncorLowerLeft:      math.Point{100, 100},
		AncorUpperRight:     math.Point{100, 300},
		AncorTypeLowerLeft:  AncorTypePixel,
		AncorTypeUpperRight: AncorTypePixel,
	}
	fills := e.Graphics.Renderer.SpriteStores["fills"]
	e.NewWindow(WindowInput, "tempInput", "tempInput", a.window, wg, fills.AtlasMap.Map["dark"].X, 12)
	w, h := e.Graphics.sdlWindow.GetSize()
	e.Windows["screen"].UpdateGeometry(math.Box{P1: math.Point{0, 0}, P2: math.Point{int(w), int(h)}}, fontDims)
	return nil
}

func (a *ActionInputWindow) SetWindow(window string) {
	a.window = window
}

func (a *ActionInputWindow) String() string {
	return "ActionInputWindow"
}
