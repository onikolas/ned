package main

// Triger scans a completed editor action and produces one or more editor actions if its conditions are met
type Trigger interface {
	Scan(action EditorAction) []EditorAction
}

// Watch a buffer and spawn an action when return is enterred
type TriggerOnReturn struct {
	Actions []EditorAction
	Buffer  string
}

func (tr TriggerOnReturn) Scan(action EditorAction) []EditorAction {
	if a, ok := action.(*ActionInsertTextAtCursor); ok && a.text[0] == rune('\n') {
		return tr.Actions
	}
	return nil
}
