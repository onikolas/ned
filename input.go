// Input provides an abtraction for input events and the mechanisms to parse input combinations.
package main

import (
	"fmt"
	"time"
)

func (k KeyType) HasText() bool {
	if k <= KeyBackspace {
		return false
	}
	return true
}

type KeyPress struct {
	Key       KeyType
	Text      string
	Timestamp time.Time
}

// InputBuffer contains a buffer for storing key presses.
// This buffer is parsed using a SyntaxTree to produce editor actions.
type InputBuffer struct {
	buffer     []KeyPress
	syntaxTree SyntaxTree
	inputTimer time.Time
}

func (in *InputBuffer) Init(size int, winType WindowType) {
	if size < 1 {
		size = 1
	}
	in.buffer = make([]KeyPress, 0, size)
	in.syntaxTree = NewSyntaxTree(winType)
}

// Adds keypress to buffer. If the current keypress completes a pattern we  return an editor action
// and true. activeBuffer is used when the action is buffer-specific (e.g. insert text)
// TODO: hardcoded inactivity period and key repeat rate
func (in *InputBuffer) Add(k KeyPress) (ea EditorAction, complete bool) {
	if k.Key == KeyNone {
		// clear input buffer after some inactivity
		t := time.Now()
		if t.Sub(in.inputTimer) > 10*time.Second && len(in.buffer) > 0 {
			in.Empty()
			fmt.Println("Input buffer cleared")
		}
		return nil, false
	}

	// drop very fast repeated inputs
	if len(in.buffer) > 0 &&
		in.buffer[len(in.buffer)-1].Key == k.Key &&
		in.buffer[len(in.buffer)-1].Text == k.Text &&
		k.Timestamp.Sub(in.buffer[len(in.buffer)-1].Timestamp) < time.Millisecond*100 {
		return nil, false
	}

	// plain text input bypasses the syntax tree for speed
	if k.Key == KeyText && len(in.buffer) == 0 {
		act := NewActionInsertTextAtCursor([]rune(k.Text))
		return act, true
	}

	in.buffer = append(in.buffer, k)
	in.inputTimer = time.Now()

	action, matchType := in.syntaxTree.Parse(in.buffer)
	if matchType == MatchFull {
		in.Empty()
		return action, true
	}
	if matchType == MatchNone {
		fmt.Println("Emptying ", in.buffer)
		in.Empty()
	}
	// partial match - dont empty buffer
	return nil, false
}

// show the buffer contents
func (in *InputBuffer) VisualizeBuffer(vb *GapBuffer) {
	vb.DeleteAll()
	for _, v := range in.buffer {
		s := fmt.Sprint(v.Key, v.Text, " > ")
		vb.InsertAtCursor([]rune(s))
	}
}

func (in *InputBuffer) Empty() {
	in.buffer = in.buffer[:0]
}
