package main

import (
	"errors"
)

// Actions go in the head and come out the tail
type ActionQueue struct {
	actions    []EditorAction
	head, tail int
}

func NewActionQueue() ActionQueue {
	return ActionQueue{actions: []EditorAction{nil}}
}

func (q *ActionQueue) Queue(action EditorAction) {
	q.actions[q.head] = action
	if q.next(q.head) == q.tail {
		q.expand()
	}
	q.head = q.next(q.head)
}

func (q *ActionQueue) DeQueue() (EditorAction, error) {
	if q.tail == q.head {
		return nil, errors.New("Queue empty")
	}
	action := q.actions[q.tail]
	q.actions[q.tail] = nil //not needed...
	q.tail = q.next(q.tail)
	return action, nil
}

func (q *ActionQueue) expand() {
	q.actions = append(q.actions, nil)

	if q.tail > q.head {
		for _, next := range q.actions[q.tail : len(q.actions)-1] {
			q.actions[q.tail+1], next = next, q.actions[q.tail+1]
			q.tail++
		}
	}
}

func (q *ActionQueue) next(index int) int {
	return (index + 1) % len(q.actions)
}
