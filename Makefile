filename=action_$(shell echo "$(name)" | tr '[:upper:]' '[:lower:]' | tr ' ' '_' ).go	
typename=Action$(shell echo "$(name)" | tr -d ' ')

build:
	go build

run:
	go build && ./ned

update-deps:
	go get -u 

clean:
	rm ./ned *~

new-action:
	printf "package main\n\n" >> $(filename)
	printf "type $(typename) struct{}\n\n" >>  $(filename)
	impl "a *$(typename)" bitbucket.org/nikolas/ned.EditorAction >>  $(filename)

new-undo-action: new-action
	impl "a *$(typename)" bitbucket.org/nikolas/ned.Undoer >> $(filename)
