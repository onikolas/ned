package main

import "runtime"

func main() {
	//lock main to the same thread - opengl expects this
	runtime.LockOSThread()
	editor := Editor{}
	editor.Init()
	editor.MainLoop()
}
