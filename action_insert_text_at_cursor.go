package main

import "fmt"

// ActionInsertTextAtCursor adds text to the curent buffer at the cursor
type ActionInsertTextAtCursor struct {
	text   []rune //text to add
	pos    int    //set to the cursor pos for undoing
	window string
}

func NewActionInsertTextAtCursor(text []rune) EditorAction {
	return &ActionInsertTextAtCursor{text, 0, ""}
}

func NewActionInsertNewlineAtCursor() EditorAction {
	return &ActionInsertTextAtCursor{[]rune("\n"), 0, ""}
}

func (a *ActionInsertTextAtCursor) Do(e *Editor, input []interface{}) []interface{} {
	buffer := e.Windows[a.window].Text
	e.Text[buffer].InsertAtCursor(a.text)
	a.pos = e.Text[buffer].gapStart
	e.AddUndoAction(a)
	return nil
}

func (a *ActionInsertTextAtCursor) Undo(e *Editor) {
	buffer := e.Windows[a.window].Text
	e.Text[buffer].MoveGap(a.pos + len(a.text))
	e.Text[buffer].DeleteBack(uint(len(a.text)))
}

func (a *ActionInsertTextAtCursor) SetWindow(window string) {
	a.window = window
}

func (a *ActionInsertTextAtCursor) String() string {
	return fmt.Sprintf("ActionInsertTextAtCursor %v", string(a.text))
}
