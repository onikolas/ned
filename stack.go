package main

import "errors"

// A simple stack
type Stack struct {
	data []interface{}
	pos  int //index to place next push (Pos-1 is the last entry)
}

func (s *Stack) Push(v interface{}) {
	if s.pos == len(s.data) {
		s.data = append(s.data, v)
	}
	s.data[s.pos] = v
	s.pos++
}

func (s *Stack) Pop() (interface{}, error) {
	if s.pos == 0 {
		return nil, errors.New("stack empty")
	}
	s.pos--
	return s.data[s.pos], nil
}

// Shrink reduces the stack so that it doesnt have unused space
func (s *Stack) Shrink() error {
	// TODO
	panic("not implemented")
}
