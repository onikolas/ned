package main

import "fmt"

type CursorDirection int

const (
	CursorUp CursorDirection = iota
	CursorDown
	CursorLeft
	CursorRight
)

// Move the cursor left or right
type ActionMoveCursor struct {
	window    string
	direction CursorDirection
}

func NewActionMoveCursorLeft() EditorAction {
	return &ActionMoveCursor{direction: CursorLeft}
}

func NewActionMoveCursorRight() EditorAction {
	return &ActionMoveCursor{direction: CursorRight}
}

func NewActionMoveCursorUp() EditorAction {
	return &ActionMoveCursor{direction: CursorUp}
}

func NewActionMoveCursorDown() EditorAction {
	return &ActionMoveCursor{direction: CursorDown}
}

func (a *ActionMoveCursor) Do(e *Editor, input []interface{}) []interface{} {
	buffer := e.Windows[a.window].Text
	text := e.Text[buffer]

	switch a.direction {
	case CursorLeft:
		text.MoveGap(text.gapStart - 1)
	case CursorRight:
		text.MoveGap(text.gapStart + 1)
	case CursorUp:
		// Move the cursor backwards until we reach a newline
		i := text.gapStart - 1
		for ; i >= 0; i-- {
			if text.buffer[i] == '\n' {
				break
			}
		}
		text.MoveGap(i)
	case CursorDown:
		// Move the cursor forward  until we reach a newline
		i := text.gapEnd
		move := 0
		for ; i < len(text.buffer); i++ {
			move++
			if text.buffer[i] == '\n' {
				break
			}
		}
		text.MoveGap(text.gapStart + move)
	}

	return nil
}

func (a *ActionMoveCursor) Undo(e *Editor) {
}

func (a *ActionMoveCursor) SetWindow(window string) {
	a.window = window
}

func (a *ActionMoveCursor) String() string {
	return fmt.Sprint("ActionMoveCursor")
}
