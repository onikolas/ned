// SDL code for setting up the window and handling low-level event capture.
package main

import (
	"fmt"
	"os"
	"time"

	"github.com/veandco/go-sdl2/sdl"
)

type Platform struct {
	// sdl
	window  *sdl.Window
	context sdl.GLContext
	event   sdl.Event

	// input
	lastModState sdl.Keymod
}

type WindowResizeEvent struct {
	width, height int
}

// Init initializes the editor window
func (p *Platform) Init(title string, winWidth, winHeight int32) {
	var err error

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}

	p.window, err = sdl.CreateWindow(title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_OPENGL|sdl.WINDOW_RESIZABLE)
	if err != nil {
		panic(err)
	}

	p.context, err = p.window.GLCreateContext()
	if err != nil {
		panic(err)
	}
}

// Quit calls appropriate teardown methods when we are done
func (p *Platform) Quit() {
	sdl.GLDeleteContext(p.context)
	p.window.Destroy()
	sdl.Quit()
}

// Handle platform events. Now handles keyboard input and window resizing events.
func (p *Platform) Events(inputChan chan KeyPress, windowChan chan WindowResizeEvent) {
	keypress := []KeyPress{}
	for {
		keypress = keypress[:0]

		event := sdl.PollEvent()
		switch t := event.(type) {

		case *sdl.QuitEvent:
			// todo: create editor  exit event
			p.Quit()
			os.Exit(0)

		case *sdl.KeyboardEvent:
			modstate := sdl.GetModState()

			if IsSet(modstate, sdl.KMOD_CTRL) && !IsSet(p.lastModState, sdl.KMOD_CTRL) {
				keypress = append(keypress, KeyPress{Key: KeyControlDown})
			}
			if !IsSet(modstate, sdl.KMOD_CTRL) && IsSet(p.lastModState, sdl.KMOD_CTRL) {
				keypress = append(keypress, KeyPress{Key: KeyControlUp})
			}

			if IsSet(modstate, sdl.KMOD_ALT) && !IsSet(p.lastModState, sdl.KMOD_ALT) {
				keypress = append(keypress, KeyPress{Key: KeyAltDown})
			}
			if !IsSet(modstate, sdl.KMOD_ALT) && IsSet(p.lastModState, sdl.KMOD_ALT) {
				keypress = append(keypress, KeyPress{Key: KeyAltUp})
			}

			// Characters in compound keypress (e.h ^C+c, ^C+v). Only activates when mod keys are active since normal
			//character input is handled by sdl.TextInputEvent. Ignoring the shift key.
			if modstate != 0 && t.Keysym.Sym >= sdl.K_a && t.Keysym.Sym <= sdl.K_z && t.Type == sdl.KEYDOWN &&
				!IsSet(p.lastModState, sdl.KMOD_SHIFT) {
				keypress = append(keypress, KeyPress{Key: KeyText, Text: string(rune(t.Keysym.Sym))})
			}

			if t.Keysym.Sym == sdl.K_RETURN && t.Type == sdl.KEYDOWN {
				keypress = append(keypress, KeyPress{Key: KeyNewline})
			}
			if t.Keysym.Sym == sdl.K_BACKSPACE && t.Type == sdl.KEYDOWN {
				keypress = append(keypress, KeyPress{Key: KeyBackspace})
			}
			if t.Keysym.Sym == sdl.K_DELETE && t.Type == sdl.KEYDOWN {
				keypress = append(keypress, KeyPress{Key: KeyDelete})
			}

			// direction keys
			if t.Keysym.Sym == sdl.K_LEFT && t.Type == sdl.KEYDOWN {
				keypress = append(keypress, KeyPress{Key: KeyLeft})
			}
			if t.Keysym.Sym == sdl.K_RIGHT && t.Type == sdl.KEYDOWN {
				keypress = append(keypress, KeyPress{Key: KeyRight})
			}
			if t.Keysym.Sym == sdl.K_UP && t.Type == sdl.KEYDOWN {
				keypress = append(keypress, KeyPress{Key: KeyUp})
			}
			if t.Keysym.Sym == sdl.K_DOWN && t.Type == sdl.KEYDOWN {
				keypress = append(keypress, KeyPress{Key: KeyDown})
			}

			p.lastModState = modstate

		case *sdl.TextInputEvent:
			keypress = append(keypress, KeyPress{Key: KeyText, Text: t.GetText()})

		case *sdl.WindowEvent:
			if t.Event == sdl.WINDOWEVENT_RESIZED {
				fmt.Printf("Window resized to: %vx%v", t.Data1, t.Data2)
				windowChan <- WindowResizeEvent{int(t.Data1), int(t.Data2)}
			}
		}

		for _, v := range keypress {
			v.Timestamp = time.Now()
			inputChan <- v
		}
	}
}

// Is this modifier (ctrl, alt, etc) set ?
func IsSet(state, mod sdl.Keymod) bool {
	return (state & mod) != 0
}
