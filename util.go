// Contains utility functions that dont fit in other files
package main

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}
