package main

import (
	"fmt"

	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/n-gl/ngl/sprite/shaders"
	"bitbucket.org/tentontrain/math"
	"github.com/veandco/go-sdl2/sdl"
)

const TAB_WIDTH = 4 //TODO parameter

var (
	DepthEditorText = sprite.Depth(500)
	DepthCursor     = sprite.Depth(300)
	DepthBackground = sprite.Depth(999)
)

type Graphics struct {
	Renderer  sprite.Renderer   // handles sprite rendering (includingh text)
	TextStore *sprite.TextStore // contains font specific rendering info
	sdlWindow *sdl.Window
}

// Initialize rendering
func (g *Graphics) Init(w, h int, window *sdl.Window) {
	// copy window pointer (need it to swap buffers)
	g.sdlWindow = window

	// OpenGL init
	g.Renderer.Init()

	// Sprite store for text rendering
	ascii := sprite.CharacterSetASCII()
	greek := sprite.CharacterSetGreek()
	both := append(ascii, greek...)
	both = append(both, '�', '↓', '↑', '⇠', '⇦', '⇨', '⇧', '⇩', '⌷', '↵', '⍰') // some special chars

	shader, err := shaders.NewSimpleSpriteShader()
	if err != nil {
		panic(err)
	}

	store, err := sprite.NewTextStore("font/NotoSansMono-Regular.ttf", shader, 24, both, TEXT_SPRITE_LIMIT, math.Point{w, h})
	if err != nil {
		panic(err)
	}
	g.TextStore = store
	g.Renderer.AddSpriteStore("text", &store.SpriteStore, 1)

	// Background fills
	spritestore, err := sprite.NewSpriteStoreFromFiles("img/fills.png", "img/fills.json", shader, TEXT_SPRITE_LIMIT, math.Point{w, h})
	if err != nil {
		panic(err)
	}
	g.Renderer.AddSpriteStore("fills", spritestore, 0)

	// Icons
	spritestore, err = sprite.NewSpriteStoreFromFiles("img/icons.png", "img/icons.json", shader, ICON_SPRITE_LIMIT, math.Point{w, h})
	if err != nil {
		panic(err)
	}
	g.Renderer.AddSpriteStore("icons", spritestore, 2)
}

func (g *Graphics) Render(e *Editor) {

	// draw window fills
	g.Renderer.SpriteStores["fills"].Empty()
	for _, w := range e.Windows {
		if w.Background < 0 {
			continue
		}
		boundingBox := w.WindowGeometry.BoundingBox
		g.Renderer.SpriteStores["fills"].AddSpriteStretched(w.Background, boundingBox.P1.X, boundingBox.P1.Y,
			boundingBox.Size().X, boundingBox.Size().Y, DepthBackground, 1)
	}

	// draw text
	characters := g.Renderer.SpriteStores["text"]
	icons := g.Renderer.SpriteStores["icons"]
	characters.Empty()
	icons.Empty()
	editorSize := e.Windows["screen"].WindowGeometry.BoundingBox.Size()

	for winName, window := range e.Windows {
		if window.Type == WindowLayout {
			continue
		}

		a, b := e.Text[window.Text].Regions(window.TextStart, e.Text[window.Text].Length())
		textRegion := [][]rune{a, b}
		runesAdded := g.TextStore.AddTextMulti(textRegion, window.WindowGeometry.BoundingBox, DepthEditorText, 1, TAB_WIDTH)
		window.TextEnd = window.TextStart + runesAdded
		g.FocusWindow(e.Text[window.Text], window)

		if e.FocusedWindow == winName {
			cursorPos := g.CursorPos(e.Text[window.Text], window)
			g.DrawCursor(cursorPos, window, editorSize)
		}

		/*if window.Type == WindowEdit {
			fmt.Printf("Window S:%v E:%v GS:%v cursor:%v runes rendered:%v\n",
				window.TextStart, window.TextEnd, e.Text[window.Text].gapStart, cursorPos, runesAdded)
		}*/
	}

	g.Renderer.Render()
	g.sdlWindow.GLSwap()
}

// pos is column, line (counted in characters) and must be translated to screen coords
func (g *Graphics) DrawCursor(pos math.Point, window *Window, editorSize math.Point) {
	fontSize := math.Point{g.TextStore.FontAtlas.HSpace, g.TextStore.FontAtlas.VSpace}
	bb := window.WindowGeometry.BoundingBox
	drawPos := math.Point{
		X: pos.X*fontSize.X + bb.P1.X,
		Y: window.WindowGeometry.BoundingBox.Size().Y - (pos.Y+1)*fontSize.Y + bb.P1.Y,
	}
	ss := g.Renderer.SpriteStores["icons"]
	ss.AddSpriteStretched(ss.AtlasMap.Map["cursor"].X, drawPos.X, drawPos.Y, fontSize.X, fontSize.Y, DepthCursor, 1)
}

// How many character fit in this window for the current font
func (g *Graphics) CharacterLimit(window *Window) int {
	// this is a crude approximation (sprites dont split)
	fontVolume := g.TextStore.FontAtlas.HSpace * g.TextStore.FontAtlas.VSpace
	windowSize := window.WindowGeometry.BoundingBox.Size()
	return windowSize.X * windowSize.Y / fontVolume
}

// Find the cursor's position in a window (measured in characters)
func (g *Graphics) CursorPos(text *GapBuffer, window *Window) math.Point {
	maxColumns := window.WindowGeometry.BoundingBox.Size().X / g.TextStore.FontAtlas.HSpace
	pos := math.Point{}
	for i := window.TextStart; i < text.Length(); i++ {
		if i == text.gapStart {
			break
		}
		switch text.At(i) {
		case '\n':
			pos.Y += 1
			pos.X = 0
		case '\t':
			pos.X += TAB_WIDTH
		default:
			pos.X += 1
		}
		if pos.X >= maxColumns {
			pos.X = 0
			pos.Y += 1
		}
	}
	return pos
}

// Make sure the rendered text includes the cursor
func (g *Graphics) FocusWindow(text *GapBuffer, window *Window) {
	if window.TextStart > text.gapStart {
		g.ScrollUp(text, window)
		fmt.Println("scroll up")
	}
	if window.TextEnd < text.gapStart {
		g.ScrollDown(text, window)
		fmt.Println("scroll down")
	}
}

func (g *Graphics) ScrollUp(text *GapBuffer, window *Window) {
	window.TextStart = text.FindLineStart(window.TextStart - 1)
}

func (g *Graphics) ScrollDown(text *GapBuffer, window *Window) {
	nl := text.FindNextLineStart(window.TextStart)
	if nl >= 0 {
		window.TextStart = nl
	}
}
