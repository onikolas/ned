module bitbucket.org/nikolas/ned

go 1.16

require (
	bitbucket.org/n-gl/ngl v0.0.0-20210922085133-5c943d02a03a
	bitbucket.org/tentontrain/math v0.0.0-20210927085912-bd393d774ae6
	github.com/go-gl/gl v0.0.0-20210905235341-f7a045908259 // indirect
	github.com/veandco/go-sdl2 v0.4.10
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
)
