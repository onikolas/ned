package main

// Get the contents of an input buffer as a string
type ActionGetBufferContents struct {
	window string
}

func NewActionBufferContents() EditorAction {
	return &ActionGetBufferContents{}
}

func (a *ActionGetBufferContents) Do(e *Editor, input []interface{}) []interface{} {
	buf := e.Text[e.Windows[a.window].Text]
	text := buf.String()
	return []interface{}{text}
}

func (a *ActionGetBufferContents) SetWindow(window string) {
	a.window = window
}

func (a *ActionGetBufferContents) String() string {
	return "ActionBufferContents"
}
