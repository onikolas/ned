package main

import "fmt"

// ActionDeleteBackwards - backspace
type ActionDeleteBackwards struct {
	text   rune //set to deleted character for undoing
	window string
	pos    int //set to the cursor pos for undoing
}

func NewActionDeleteBackwards() EditorAction {
	return &ActionDeleteBackwards{}
}

func (a *ActionDeleteBackwards) Do(e *Editor, input []interface{}) []interface{} {
	buffer := e.Windows[a.window].Text
	a.pos = e.Text[buffer].gapStart - 1
	if a.pos >= 0 {
		a.text = e.Text[buffer].At(a.pos)
		e.Text[buffer].DeleteBack(1)
		e.UndoBuffer.Push(a)
	}
	return nil
}

func (a *ActionDeleteBackwards) Undo(e *Editor) {
	buffer := e.Windows[a.window].Text
	e.Text[buffer].Insert([]rune{a.text}, a.pos)
}

func (a *ActionDeleteBackwards) SetWindow(w string) {
	a.window = w
}

func (a *ActionDeleteBackwards) String() string {
	return fmt.Sprintln("ActionDeleteBackwards", a.pos, string(a.text))
}
