package main

import "testing"

func TestGapSearchRune(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(10)
	gb.Insert([]rune("abc\ndef"), 0)
	gb.MoveGap(4)

	tests := []struct {
		pos int
		c   int
	}{
		{0, 3},
		{1, 3},
		{2, 3},
		{3, 3},
		{4, -1},
		{-1, -1},
		{6, -1},
		{7, -1},
	}

	for _, test := range tests {
		next := gb.SearchRune('\n', test.pos)
		if next != test.c {
			t.Error(test, next)
		}
	}

	tests = []struct {
		pos int
		c   int
	}{
		{0, -1},
		{1, -1},
		{2, -1},
		{3, 3},
		{4, 3},
		{-1, -1},
		{6, 3},
		{7, -1},
	}

	for _, test := range tests {
		next := gb.SearchRuneReverse('\n', test.pos)
		if next != test.c {
			t.Error(test, next)
		}
	}
}

func TestGapFindLineStart(t *testing.T) {
	text := "\n" + // 0
		"The quick brown fox\n" + //1-20
		"jumps over the lazy dog" //21-43
	gb := GapBuffer{}
	gb.Init(10)
	gb.Insert([]rune(text), 0)

	tests := []struct {
		pos int
		c   int
	}{
		{0, 0},
		{-1, 0},
		{1, 1},
		{2, 1},
		{6, 1},
		{20, 1},
		{21, 21},
		{31, 21},
		{51, 21},
	}

	for _, test := range tests {
		start := gb.FindLineStart(test.pos)
		if start != test.c {
			t.Error(test, start)
		}
	}

	tests = []struct {
		pos int
		c   int
	}{
		{0, 1},
		{-1, -1},
		{1, 21},
		{2, 21},
		{6, 21},
		{20, 21},
		{21, -1},
		{31, -1},
		{51, -1},
	}

	for _, test := range tests {
		start := gb.FindNextLineStart(test.pos)
		if start != test.c {
			t.Error(test, start)
		}
	}
}
