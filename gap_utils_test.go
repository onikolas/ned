package main

import "testing"

func TestHasALine(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(10)
	gb.InsertAtCursor([]rune("Asdf"))
	if HasNewLine(&gb) {
		t.Error(gb)
	}
	gb.InsertAtCursor([]rune("\n"))
	if !HasNewLine(&gb) {
		t.Error(gb)
	}
}
