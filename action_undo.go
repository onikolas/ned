package main

// ActionUndo calls undo for the latest entry in the editor's undo buffer
type ActionUndo int

func NewActionUndo() EditorAction {
	a := ActionUndo(0)
	return &a
}

func (a *ActionUndo) Do(e *Editor, in []interface{}) []interface{} {
	e.Undo()
	return nil
}

func (a *ActionUndo) Undo(e *Editor) {
}

func (a *ActionUndo) SetWindow(wy string) {
	// no need to store as each action in the undo buffer will have this info
}

func (a ActionUndo) String() string {
	return "Undo!"
}
