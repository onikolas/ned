package main

import "testing"

func TestSyntaxTreeAdd(t *testing.T) {
	st := NewSyntaxTreeEmpty()
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "c"}}, nil)
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "v"}}, nil)
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "c"}, {Key: KeyText, Text: "a"}},
		NewActionInsertDummyText)
	st.Add([]KeyPress{{Key: KeyAltDown, Text: ""}}, NewActionInsertDummyText)

	if st.Root.Children[0].Key.Key != KeyControlDown || len(st.Root.Children) != 2 {
		t.Error(st.Root)
	}

	if st.Root.Children[0].Children[0].Key.Key != KeyText ||
		st.Root.Children[0].Children[0].Key.Text != "c" {
		t.Error(st.Root.Children[0].Children[0].Key)
	}

	if st.Root.Children[0].Children[1].Key.Key != KeyText ||
		st.Root.Children[0].Children[1].Key.Text != "v" {
		t.Error(st.Root.Children[0].Children[1].Key)
	}

	if st.Root.Children[0].Children[0].Key.Key != KeyText ||
		st.Root.Children[0].Children[0].Key.Text != "c" ||
		st.Root.Children[0].Children[0].Children[0].Key.Text != "a" {
		t.Error(st.Root.Children[0].Children[1].Key)
	}

	if st.Root.Children[1].Key.Key != KeyAltDown {
		t.Error(st.Root)
	}

	if _, ok := st.Root.Children[1].NewAction().(*ActionInsertTextAtPos); !ok {
		t.Error(st.Root)
	}
}

func TestSyntaxTreeParse(t *testing.T) {
	st := NewSyntaxTreeEmpty()
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "c"}}, nil)
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "v"}}, nil)
	st.Add([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "c"}, {Key: KeyText, Text: "a"}},
		NewActionInsertDummyText)
	st.Add([]KeyPress{{Key: KeyAltDown, Text: ""}}, NewActionInsertDummyText)

	// test a full match
	action, match := st.Parse([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "c"}, {Key: KeyText, Text: "a"}})
	if match != MatchFull {
		t.Error("expected full match here")
	}
	if _, ok := action.(*ActionInsertTextAtPos); !ok {
		t.Error("bad action")
	}

	// test a partial match
	action, match = st.Parse([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyText, Text: "c"}})
	if match != MatchPartial || action != nil {
		t.Error("expected partial match here")
	}

	// test no match
	action, match = st.Parse([]KeyPress{{Key: KeyControlDown, Text: ""}, {Key: KeyControlUp, Text: ""}})
	if match != MatchNone || action != nil {
		t.Error("expected no match here")
	}
}
