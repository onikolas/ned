// Enumeration of key inputs
package main

type KeyType int

const (
	// These only control
	KeyNone KeyType = iota
	KeyControlDown
	KeyControlUp
	KeyAltDown
	KeyAltUp
	KeyBackspace
	KeyDelete
	KeyLeft
	KeyRight
	KeyUp
	KeyDown
	// These have text
	KeySpace
	KeyNewline
	KeyText
)

var keyTypeToString = map[KeyType]string{
	KeyNone:        "N/A",
	KeyControlDown: "Cd",
	KeyControlUp:   "Cu",
	KeyAltDown:     "Ad",
	KeyAltUp:       "Au",
	KeyBackspace:   "BS",
	KeyLeft:        "L",
	KeyRight:       "R",
	KeyUp:          "U",
	KeyDown:        "D",
	KeySpace:       "SP",
	KeyNewline:     "NL",
	KeyText:        "TXT",
}

func (k KeyType) String() string {
	return keyTypeToString[k]
}
