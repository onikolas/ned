package main

import "fmt"

type ActionDebugDumbDump struct {
	window string
}

func NewActionDebugDumbDumps() EditorAction {
	return &ActionDebugDumbDump{}
}

func (a *ActionDebugDumbDump) Do(e *Editor, input []interface{}) []interface{} {
	fmt.Printf("TEXT \n %+v \n\n", e.Text)
	fmt.Printf("WINDOWS\n %+v \n\n", e.Windows)
	fmt.Printf("FocusedWindow: %+v PrevFocusedWindow:%+v \n\n", e.FocusedWindow, e.PrevFocusedWindow)
	return nil
}

func (a *ActionDebugDumbDump) SetWindow(window string) {}

func (a *ActionDebugDumbDump) String() string {
	return "ActionDebugDumbDump"
}
