package main

import "os"

// Load a file from disk
type ActionQuit struct{}

// Quit NED
func NewActionQuit() EditorAction {
	return &ActionQuit{}
}

func (a *ActionQuit) Do(e *Editor, in []interface{}) []interface{} {
	// TODO check for unsaved files and do cleanup when needed
	os.Exit(0)
	return nil
}

func (a *ActionQuit) SetWindow(w string) {}

func (a *ActionQuit) String() string {
	return "ActionQuit"
}
