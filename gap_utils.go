// Functions that manipulate text stored in gap buffers
package main

// HasNewLine returns true if there is a newline in this buffer. Used to detect input
// from buffers used for temporary user input
func HasNewLine(gb *GapBuffer) bool {
	for i := 0; i < gb.Length(); i++ {
		if gb.At(i) == '\n' {
			return true
		}
	}
	return false
}
