// Editor organises and coordinates the main components of the system: IO (sdl), text (gap buffer)
// and display (OpenGL). Accessing the editor' state (text buffers, windows, etc) is not thread-safe and
// concurrent operations must be serialized through the editor's function chanel
package main

import (
	"bitbucket.org/tentontrain/math"
)

const (
	TEXT_SPRITE_LIMIT = 4096
	ICON_SPRITE_LIMIT = 1024
	BG_SPRITE_LIMIT   = 64
	RENDER_ON_INPUT   = false
)

// Editor consists of input, storage and rendering of text.
type Editor struct {
	Platform Platform              // window creation and I/O
	Text     map[string]*GapBuffer // named buffers for holding text
	Windows  map[string]*Window    // windows that display the contents of buffers
	Graphics Graphics              // renders stuff

	Actions  ActionQueue // holds actions to be executed
	Triggers []Trigger   // conditional actions

	FocusedWindow     string
	PrevFocusedWindow string

	// undo for the whole editor (instead of per window)
	UndoBuffer Stack
}

// Add to undo buffer
func (e *Editor) AddUndoAction(a EditorAction) {
	e.UndoBuffer.Push(a)
}

// Undo last action
func (e *Editor) Undo() {
	action, err := e.UndoBuffer.Pop()
	if err == nil {
		action.(Undoer).Undo(e)
	}
}

// Read input, update text buffer, render
func (e *Editor) MainLoop() {
	// read events and buffer in chan
	inputChan := make(chan KeyPress, 64)
	windowChan := make(chan WindowResizeEvent, 64)
	go e.Platform.Events(inputChan, windowChan)

	for {
		if e.Windows[e.FocusedWindow].Type == WindowEdit {
			e.Windows[e.FocusedWindow].Input.VisualizeBuffer(e.Text["bar"])
		}

		// handle inputs
		select {
		case input := <-inputChan:
			action, complete := e.Windows[e.FocusedWindow].Input.Add(input)
			if complete {
				action.SetWindow(e.FocusedWindow)
				e.Actions.Queue(action)

				if RENDER_ON_INPUT {
					e.Graphics.Render(e)
				}
			}
		default:
			// pass noop because input needs to run even if nothing happened (to clear buffers, reset timers etc)
			e.Windows[e.FocusedWindow].Input.Add(KeyPress{Key: KeyNone})
		}

		// handle window events like resizing
		select {
		case windowEvent := <-windowChan:
			e.Graphics.Renderer.Resize(windowEvent.width, windowEvent.height)
			wh := math.Point{X: windowEvent.width, Y: windowEvent.height}
			e.Windows["screen"].UpdateGeometry(math.Box{P1: math.Point{0, 0}, P2: wh},
				math.Point{e.Graphics.TextStore.FontAtlas.HSpace, e.Graphics.TextStore.FontAtlas.VSpace})
		default:
		}

		// Execute actions
		if action, err := e.Actions.DeQueue(); err == nil {
			action.Do(e, nil)
		}

		if !RENDER_ON_INPUT {
			e.Graphics.Render(e)
		}
	}
}

// Initialize the editor. This mess is temporary, we should load all from config files
func (e *Editor) Init() {
	w, h := 1000, 800
	e.initBuffers()
	e.Platform.Init("NED", int32(w), int32(h))
	e.Graphics.Init(w, h, e.Platform.window)
	fontDims := math.Point{e.Graphics.TextStore.FontAtlas.HSpace, e.Graphics.TextStore.FontAtlas.VSpace}
	e.initWindows(math.Point{w, h}, fontDims)
	e.FocusedWindow = "default"
	e.Actions = NewActionQueue()
}

// initialize default buffers
func (e *Editor) initBuffers() {
	e.Text = make(map[string]*GapBuffer)
	e.Text["default"] = NewGapBuffer(DefaultChunkSize)
	e.Text["bar"] = NewGapBuffer(DefaultChunkSize)
	e.Text["tempInput"] = NewGapBuffer(DefaultChunkSize)
}

// Creates a new editor window
func (e *Editor) NewWindow(winType WindowType, name, text, parent string, wg WindowGeometry, background, depth int) {
	e.Windows[name] = NewWindow(winType, name, text, wg, background, depth)
	if _, ok := e.Windows[parent]; ok {
		e.Windows[parent].AddChildWindow(e.Windows[name])
	}
}

// initialize default windows
func (e *Editor) initWindows(windowSize math.Point, fontDims math.Point) {
	e.Windows = make(map[string]*Window)
	fills := e.Graphics.Renderer.SpriteStores["fills"]

	wg := WindowGeometry{
		AncorLowerLeft:      math.Point{0, 0},
		AncorUpperRight:     math.Point{0, 0},
		AncorTypeLowerLeft:  AncorTypePixel,
		AncorTypeUpperRight: AncorTypePixel,
	}
	e.NewWindow(WindowLayout, "screen", "", "", wg, -1, 0)

	wg = WindowGeometry{
		AncorLowerLeft:      math.Point{0, fontDims.Y},
		AncorUpperRight:     math.Point{100, 100},
		AncorTypeLowerLeft:  AncorTypePixel,
		AncorTypeUpperRight: AncorTypePercent,
	}
	e.NewWindow(WindowEdit, "default", "default", "screen", wg, fills.AtlasMap.Map["grey"].X, 0)

	wg = WindowGeometry{
		AncorLowerLeft:      math.Point{0, 0},
		AncorUpperRight:     math.Point{100, fontDims.Y},
		AncorTypeLowerLeft:  AncorTypePixel,
		AncorTypeUpperRight: AncorTypeHStretch,
	}
	e.NewWindow(WindowInput, "bar", "bar", "screen", wg, fills.AtlasMap.Map["dark"].X, 0)

	e.Windows["screen"].UpdateGeometry(math.Box{P1: math.Point{0, 0}, P2: windowSize}, fontDims)
}

// Change focus to window. Stores the old buffer to be able to go back
func (e *Editor) ChangeFocus(window string) {
	e.PrevFocusedWindow, e.FocusedWindow = e.FocusedWindow, window
}

func (e *Editor) SwapFocus() {
	e.PrevFocusedWindow, e.FocusedWindow = e.FocusedWindow, e.PrevFocusedWindow
}
