// List the editor's buffers
package main

import "fmt"

type ActionListBuffers int

func NewActionListBuffers() EditorAction {
	return ActionListBuffers(41)
}

func (a ActionListBuffers) Do(e *Editor, input []interface{}) []interface{} {
	// switch to default text buffer and clear it
	e.Windows["default"].Text = "default"
	e.Text["default"].DeleteAll()
	for name := range e.Text {
		l := " * " + name + "\n"
		e.Text["default"].InsertAtCursor([]rune(l))
	}
	return nil
}

func (a ActionListBuffers) SetWindow(window string) {}

func (a ActionListBuffers) String() string {
	return fmt.Sprint("ActionListBuffers")
}
