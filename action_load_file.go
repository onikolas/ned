package main

import (
	"fmt"
	"os"
	"unicode/utf8"
)

// Load a file from disk
type ActionOpenFile struct {
	filename, window string
}

// If action is created with a filename it will be loaded automatically. Passing "" will prompt the user for the filename.
func NewActionOpenFile() EditorAction {
	return &ActionOpenFile{filename: ""}
}

func (a *ActionOpenFile) Do(e *Editor, input []interface{}) []interface{} {
	if a.filename != "" {
		a.Load(e)
		return nil
	}
	//text := e.InputWindow("Filename: ")
	//e.Triggers = append(e.Triggers, TriggerOnReturn{NewActionOpenFile()})
	return nil
}

func (a *ActionOpenFile) Load(e *Editor) {
	file, err := os.OpenFile(a.filename, os.O_RDWR|os.O_CREATE, 0644)
	defer file.Close()

	// report error to status bar
	if err != nil {
		fmt.Println(err)
		fmt.Println(os.Getwd())
		e.Text["bar"].InsertAtCursor([]rune(err.Error()))
		return
	}

	// TODO: check if its a text file!

	// make a new text buffer
	e.Text[a.filename] = NewGapBuffer(DefaultChunkSize)

	bytes := make([]byte, 1024) // read in 1KB chunks
	runes := make([]rune, 0, 1024)
	for nbytes, err := file.Read(bytes); err == nil; nbytes, err = file.Read(bytes) {
		n := 0
		for n < nbytes {
			ru, size := utf8.DecodeRune(bytes[n:])
			n += size
			runes = append(runes, ru)
		}
		e.Text[a.filename].InsertAtCursor(runes)
		runes = runes[:0]
	}
	e.Windows[a.window].Text = a.filename
	e.Text[a.filename].MoveGap(0)
	e.FocusedWindow = a.window
	fmt.Println(a.window)
}

func (a *ActionOpenFile) SetWindow(w string) {
	a.window = w
}

func (a *ActionOpenFile) String() string {
	return "ActionOpenFile"
}
