package main

import "fmt"

type ActionSwitchBuffer struct {
	window string
}

func NewActionSwitchBuffer() EditorAction {
	return &ActionSwitchBuffer{}
}

func (a *ActionSwitchBuffer) Do(e *Editor, in []interface{}) []interface{} {
	// NewActionListBuffers().Do(e, nil)
	// go func() {
	// 	ch := e.InputWindow("Buffer: ")
	// 	buffer := <-ch
	// 	a.Switch(buffer, e)
	// }()
	return nil
}

func (a *ActionSwitchBuffer) Switch(name string, e *Editor) {
	if _, ok := e.Text[name]; !ok {
		return
	}
	if _, ok := e.Windows[a.window]; !ok {
		panic(a.window)
	}
	e.Windows[a.window].Text = name
}

func (a *ActionSwitchBuffer) SetWindow(w string) {
	a.window = w
}

func (a *ActionSwitchBuffer) String() string {
	return fmt.Sprint("ActionSwitchBuffer")
}
