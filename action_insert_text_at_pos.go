package main

import "fmt"

// ActionInsertTextAtPos adds text to the current buffer at specified position.
type ActionInsertTextAtPos struct {
	text   []rune //text to add
	window string
	pos    int //position to add it
}

func NewActionInsertTextAtPos(text []rune, pos int) *ActionInsertTextAtPos {
	return &ActionInsertTextAtPos{text, "", pos}
}

func NewActionInsertDummyText() EditorAction {
	return &ActionInsertTextAtPos{[]rune("DUMMY"), "", 0}
}

func (a *ActionInsertTextAtPos) Do(e *Editor, input []interface{}) []interface{} {
	buffer := e.Windows[a.window].Text
	e.Text[buffer].Insert(a.text, a.pos)
	e.AddUndoAction(a)
	return nil
}

func (a *ActionInsertTextAtPos) Undo(e *Editor) {
	buffer := e.Windows[a.window].Text
	e.Text[buffer].MoveGap(a.pos + len(a.text))
	e.Text[buffer].DeleteBack(uint(len(a.text)))
}

func (a *ActionInsertTextAtPos) SetWindow(window string) {
	a.window = window
}

func (a *ActionInsertTextAtPos) String() string {
	return fmt.Sprintf("ActionInsertTextAtPos %v", string(a.text))
}
