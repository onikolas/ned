package main

import "fmt"

// ActionDeleteBackwards - backspace
type ActionDeleteForward struct {
	text   rune //set to deleted character for undoing
	window string
	pos    int //set to the cursor pos for undoing
}

func NewActionDeleteForward() EditorAction {
	return &ActionDeleteForward{}
}

func (a *ActionDeleteForward) Do(e *Editor, input []interface{}) []interface{} {
	buffer := e.Windows[a.window].Text
	a.text = e.Text[buffer].At(e.Text[buffer].gapStart)
	a.pos = e.Text[buffer].gapStart
	e.Text[buffer].DeleteForward(1)
	e.AddUndoAction(a)
	return nil
}

func (a *ActionDeleteForward) Undo(e *Editor) {
	buffer := e.Windows[a.window].Text
	e.Text[buffer].Insert([]rune{a.text}, a.pos)
}

func (a *ActionDeleteForward) SetWindow(w string) {
	a.window = w
}

func (a *ActionDeleteForward) String() string {
	return fmt.Sprintln("ActionDeleteForward", a.pos, string(a.text))
}
