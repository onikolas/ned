package main

import (
	"testing"
)

// Common check for all tests.
// Check that gb contains text and gap starts at gs and ends at ge
func BuffTest(gb *GapBuffer, text string, gs, ge int) bool {
	if gb.String() == text && gb.gapStart == gs && gb.gapEnd == ge {
		return true
	}
	return false
}

// Test text insertion into buffer
func TestGapBufferInsert(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(10)

	// into empty buffer
	gb.Insert([]rune("re"), 0)
	gb.Insert([]rune("m"), 2)
	if !BuffTest(&gb, "rem", 3, 10) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	// at the buffer's start
	gb.Insert([]rune("Lo"), 0)
	if !BuffTest(&gb, "Lorem", 2, 7) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	// forward movement
	gb.Insert([]rune("O"), 1)
	if !BuffTest(&gb, "LOorem", 2, 6) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	gb.Insert([]rune("R"), 3)
	if !BuffTest(&gb, "LOoRrem", 4, 7) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	// Insert without moving the gap
	gb.Insert([]rune("R"), 4)
	if !BuffTest(&gb, "LOoRRrem", 5, 7) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	// Backwards movement
	gb.Insert([]rune(" "), 0)
	if !BuffTest(&gb, " LOoRRrem", 1, 2) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	// Insufficient space (should resize)
	gb.resizeChunk = 10
	gb.Insert([]rune(" Ipsum"), 9)
	if !BuffTest(&gb, " LOoRRrem Ipsum", 15, 20) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}
}

// Test gap movement
func TestGapBufferMoveGap(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(10)
	gb.Insert([]rune("abcdef"), 0)

	if !BuffTest(&gb, "abcdef", 6, 10) {
		t.Error("Bad init ", gb.String(), gb.gapStart, gb.gapEnd)
	}

	// left movement
	gb.MoveGap(3)
	if s := gb.String(); s != "abcdef" {
		t.Error("bad string ", s)
	}
	if gb.gapStart != 3 || gb.gapEnd != 7 {
		t.Error("bad gap pos ", gb.gapStart, gb.gapEnd)
	}

	// left movement
	gb.MoveGap(0)
	if s := gb.String(); s != "abcdef" {
		t.Error("bad string ", s)
	}
	if gb.gapStart != 0 || gb.gapEnd != 4 {
		t.Error("bad gap pos ", gb.gapStart, gb.gapEnd)
	}

	// right movement
	gb.MoveGap(4)
	if s := gb.String(); s != "abcdef" {
		t.Error("bad string ", s)
	}
	if gb.gapStart != 4 || gb.gapEnd != 8 {
		t.Error("bad gap pos ", gb.gapStart, gb.gapEnd)
	}

	// right movement
	gb.MoveGap(6)
	if s := gb.String(); s != "abcdef" {
		t.Error("bad string ", s)
	}
	if gb.gapStart != 6 || gb.gapEnd != 10 {
		t.Error("bad gap pos ", gb.gapStart, gb.gapEnd)
	}

	// invalid movements
	if gb.MoveGap(-2) != false {
		t.Error("no error")
	}
	if s := gb.String(); s != "abcdef" {
		t.Error("bad string ", s)
	}
	if gb.gapStart != 6 || gb.gapEnd != 10 {
		t.Error("bad gap pos ", gb.gapStart, gb.gapEnd)
	}

	if gb.MoveGap(7) != false {
		t.Error("no error")
	}
	if s := gb.String(); s != "abcdef" {
		t.Error("bad string ", s)
	}
	if gb.gapStart != 6 || gb.gapEnd != 10 {
		t.Error("bad gap pos ", gb.gapStart, gb.gapEnd)
	}
}

// Test proper indexing
func TestGapBufferValidPos(t *testing.T) {

	gb := GapBuffer{}
	gb.Init(13)
	gb.Insert([]rune("ASDF"), 0)

	tests := []struct {
		pos     int
		correct bool
	}{
		{-3, false},
		{0, true},
		{3, true},
		{4, true}, //!!!
		{5, true}, //!!!
		{13, false},
		{55, false},
	}

	for _, v := range tests {
		if gb.ValidPos(v.pos) != v.correct {
			t.Error(v, gb.GapSize())
		}
	}
}

// Print should return the users view of the buffer which excludes the gap
func TestGapBufferPrint(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(9)
	gb.buffer[0] = 'A'
	gb.buffer[1] = 'S'
	gb.buffer[2] = 'D'
	gb.buffer[6] = 'F'
	gb.buffer[7] = 'G'
	gb.buffer[8] = 'H'
	gb.gapStart = 3
	gb.gapEnd = 6

	if s := gb.String(); s != "ASDFGH" {
		t.Error(s)
	}
}

func TestGapBufferDelete(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(9)

	// full buffer
	gb.Insert([]rune("LoRRpppem"), 0)
	if !BuffTest(&gb, "LoRRpppem", 9, 9) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	// delete backwards 2
	gb.MoveGap(4)
	gb.DeleteBack(2)
	if !BuffTest(&gb, "Lopppem", 2, 4) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	// delete forward 3
	gb.DeleteForward(3)
	if !BuffTest(&gb, "Loem", 2, 7) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	gb.Insert([]rune("r"), gb.gapStart)
	if !BuffTest(&gb, "Lorem", 3, 7) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}
}

func TestGapBufferResize(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(10)
	gb.Insert([]rune("Lorem"), 0)
	if !BuffTest(&gb, "Lorem", 5, 10) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	gb.Resize(-3)
	if !BuffTest(&gb, "Lorem", 5, 7) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	gb.Resize(-10)
	if !BuffTest(&gb, "Lorem", 5, 5) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	gb.Resize(12)
	if !BuffTest(&gb, "Lorem", 5, 17) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}
}

func TestGapBufferText(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(0)
	gb.Insert([]rune("Ipsum"), 0)
	gb.Insert([]rune("Lorem "), 0)

	if !BuffTest(&gb, "Lorem Ipsum", 6, 251) {
		t.Error(gb.String(), gb.gapStart, gb.gapEnd)
	}

	p1, p2 := gb.Text()
	if string(p1) != "Lorem " {
		t.Error(string(p1))
	}

	if string(p2) != "Ipsum" {
		t.Error(string(p2))
	}
}

func TestGapLoop(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(20)
	gb.Insert([]rune("Lorem Ipsum"), 0)

	for pos := 0; pos < gb.Length(); pos++ {
		gb.MoveGap(pos)
		s := ""
		for i := 0; i < gb.Length(); i++ {
			s += string(gb.At(i))
		}

		if s != gb.String() {
			t.Errorf("length=%v gS=%v gE=%v s=%v text=%v\n", gb.Length(), gb.gapStart, gb.gapEnd, s, gb)
		}
	}
}

func TestGapLength(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(20)
	gb.Insert([]rune("411 "), 0)

	if l := gb.Length(); l != 4 {
		t.Error(l)
	}
}

func TestGapRegions(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(10)
	gb.Insert([]rune("abcdef"), 0)
	gb.MoveGap(0)

	a, b := gb.Regions(1, 3)
	if string(a) != "" || string(b) != "bc" {
		t.Error(string(a), string(b))
	}

	a, b = gb.Regions(1, 5)
	if string(a) != "" || string(b) != "bcde" {
		t.Error(string(a), string(b))
	}

	a, b = gb.Regions(1, 4)
	if string(a) != "" || string(b) != "bcd" {
		t.Error("a:", string(a), "b:", string(b))
	}

	gb.MoveGap(3)

	a, b = gb.Regions(1, 3)
	if string(a) != "bc" || len(b) != 0 {
		t.Error(string(a), string(b))
	}

	a, b = gb.Regions(1, 5)
	if string(a) != "bc" || string(b) != "de" {
		t.Error(string(a), string(b))
	}

	a, b = gb.Regions(1, 4)
	if string(a) != "bc" || string(b) != "d" {
		t.Error(string(a), string(b))
	}

	a, b = gb.Regions(4, 8)
	if len(a) != 0 || string(b) != "ef" {
		t.Error(string(a), string(b))
	}

	a, b = gb.Regions(0, gb.Length())
	if string(a) != "abc" || string(b) != "def" {
		t.Error(string(a), string(b))
	}

	if ok := gb.MoveGap(6); !ok {
		t.Error("bad move... punk")
	}

	a, b = gb.Regions(1, 3)
	if string(a) != "bc" || len(b) != 0 {
		t.Error(string(a), string(b))
	}

	a, b = gb.Regions(1, 3)
	if string(a) != "bc" || len(b) != 0 {
		t.Error(string(a), string(b))
	}
}

func TestGapConstrainIndex(t *testing.T) {
	gb := GapBuffer{}
	gb.Init(10)
	gb.Insert([]rune("abcdef"), 0)
	gb.MoveGap(3)

	tests := []struct {
		index, result int
	}{
		{-10, 0}, {-1, 0},
		{0, 0}, {1, 1},
		{4, 4}, {5, 5},
		{6, 5}, {7, 5},
	}

	for _, v := range tests {
		if a := gb.ConstrainIndex(v.index); a != v.result {
			t.Error(a, v, gb.Length())
		}
	}
}
